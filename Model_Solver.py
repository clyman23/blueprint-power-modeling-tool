# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 10:17:01 2018

@author: clyma
"""

import pyomo.environ as pyo


class Horizon_Solver:
    """
    Class for maximizing revenue in given time horizon
    
    Horizon_Solver models the revenue generation for a battery storage system
    performing energy arbitrage in the NYISO day ahead energy market
    
    Parameters:
    horizon (int): time horizon for optimization in hours
    cost_data (dict): LBMP in $/MWHr from NYISO data
    time_data (list): Possibly don't need
    initial_values (dict): Initial values for d, c, and s
    """
    
    def __init__(self, horizon, cost_data, initial_values, day):
        self.horizon = horizon
        self.cost_data = cost_data
        #self.time_data = time_data
        self.initial_values = initial_values
        self.day = day
        
    def create_model(self):
        model = pyo.ConcreteModel()
        horizon = self.horizon
        model.T = pyo.RangeSet(1+horizon*(self.day-1),horizon*self.day)
        cost = self.cost_data
        initial_values = self.initial_values
        model.p = pyo.Param(model.T, initialize=cost, default = 0)
        model.d = pyo.Var(model.T, domain=pyo.NonNegativeReals, initialize=initial_values['d'])
        model.c = pyo.Var(model.T, domain=pyo.NonNegativeReals, initialize=initial_values['c'])
        model.s = pyo.Var(model.T, domain=pyo.NonNegativeReals, initialize=initial_values['s'])

        def d_constraint1(model, t):
            max_d = 200
            return model.d[t] <= max_d and model.d[t] >= 0

        def d_constraint2(model, t):
            max_daily_d = 200
            return pyo.summation(model.d) <= max_daily_d 

        model.d_con1 = pyo.Constraint(model.T, rule=d_constraint1)
        model.d_con2 = pyo.Constraint(model.T, rule=d_constraint2)
        
        def c_constraint1(model, t):
            max_c = 100
            return model.c[t] <= max_c and model.c[t]>=0
        
        model.c_con1 = pyo.Constraint(model.T, rule=c_constraint1)

        def s_constraint1(model, t):
            efficiency = 0.85
            s_start = 1+self.horizon*(self.day-1)
            s_end = self.horizon*self.day
            if t == s_start:
                return model.s[t] == efficiency*model.c[t]-model.d[t]
            elif t == s_end:
                return model.s[t-1]+efficiency*model.c[t]-model.d[t] == model.s[s_start]
            return model.s[t] == model.s[t-1]+efficiency*model.c[t]-model.d[t]
        
        model.s_con1 = pyo.Constraint(model.T, rule=s_constraint1)

        #def s_constraint2(model, t):
            
        
        model.OBJ = pyo.Objective(expr=sum((model.p[t]/1000)*(model.d[t]-model.c[t]) for t in model.T), \
                                  sense=pyo.maximize)
        
        #instance = model.create_instance() # DEPRECATED!!!
        
        return model
    
    def solve_model(self, model):
        opt = pyo.SolverFactory('glpk')
        results = opt.solve(model)
        
        #Optional printing
        '''model.pprint()
        print('\n--------------------------------------------------------\n')
        results.write()
        print('\n--------------------------------------------------------\n')
        model.display()'''
        return model

'''
def main(horizon, cost_data, initial_values):
    solving = Horizon_Solver(horizon, cost_data, initial_values)
    results = solving.solve_model(solving)
    

if __name__ == '__main__':
    main(horizon, cost_data, initial_values)

'''




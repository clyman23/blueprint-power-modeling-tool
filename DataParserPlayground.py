# -*- coding: utf-8 -*-
"""
Created on Sat Nov  3 12:29:18 2018

Parse out energy cost data for NYC

@author: clyma
"""
import os
import csv
import matplotlib.pyplot as plt

class ReadData():
    
    
    '''
    def read_csvs(self, files):
        all_data = []
        for i in files:
            all_data.append(read_csv(i))
        return all_data
    '''
    
    def read_csv(self, file):
        data = []
        with open(file, 'r') as csvfile:
            contents = csv.DictReader(csvfile)
            for row in contents:
                if row['Name'] == 'N.Y.C.':
                    value = float(row.pop('LBMP ($/MWHr)'))
                    data.append(value)
        return data
    
    def get_files(self, location):
        files = os.listdir(location)
        try: 
            files.remove('.DS_Store')
        except:
            pass
        for i in range(0,len(files)):
            files[i] = os.path.join(location, files[i])
        return files
    
    def get_file_dirs(self, location):
        dirs = os.listdir(location)
        try:
            dirs.remove('.DS_Store')
        except:
            pass
        for i in range(0,len(dirs)):
            dirs[i] = os.path.join(location, dirs[i])
        return dirs
    
    def extract_NYC_data(self, file_location):
        dirs = self.get_file_dirs(file_location)
        all_data = []
        all_files = []
        for i in dirs:
            all_files.extend(self.get_files(i))
        for i in all_files:
            all_data.extend(self.read_csv(i))
        return all_data
    
    def re_write_data(self, file_location, all_data):
        print("exporting data")
        file = os.path.join(file_location, 'All_Data.csv')
        with open(file, 'w') as fh:
            wr = csv.writer(fh)
            wr.writerow(all_data)
    
    def plot_data(self, all_data):
        time = []
        cost = []
        '''for i in all_data:
            time.append(i.get('Time Stamp'))
            cost.append(float(i.get('LBMP ($/MWHr)')))
            '''
        time = range(1,len(all_data)+1)
        cost = all_data
        print("Data sorted...")
        plt.plot(time, cost)
        plt.show()


#file_location = r'C:\Users\clyma\Documents\BlueprintPower_Challenge\2017_NYISO_LBMPs'
'''        
#dirs = get_file_dirs(file_location)
#files = get_files(dirs[0])
#data = read_csvs(files)
all_data = extract_NYC_data(file_location)
plot_data(all_data)'''
#This is a comment

file_location = r'C:\Users\clyma\Documents\BlueprintPower_Challenge\2017_NYISO_LBMPs'

data_reader = ReadData()

all_data = data_reader.extract_NYC_data(file_location)

data_reader.re_write_data(file_location, all_data)

Blueprint Power Modeling Challenge

***Note: recognize this README was pushed after the deadline. Other changes made after the deadline were for housekeeping purposes only***

The solution is found and results are returned by running the file Solution.py
	
	python Solution.py

This file makes use of custom functions from 'Model_Solver.py', 'DataReader.py', and 'Full_Solver.py'.

The solution was developed and run in a conda environment using the latest version of Python 3. Libraries used included csv, os, matplotlib, and pyomo. 

'Model_Solver.py' optimizes the revenue generated from the energy arbitrage system for a given time horizon. All resources used to develop the model can be found in the "Sources" directory. 

The provided cost data for LBMP was parsed out of the source csv files and written out to a single csv, 'All_Data.csv', using the code 'DataParser.py'. This is certainly the crudest of the Python files. It was assumed that the goal of the project was developing the optimization model and delivering the results; therefore, little time was spent on parsing out the cost data. 

The cost data was read into the model using 'DataReader.py'.

'Full_Solver.py' was written to run 'Model_Solver.py' for multiple days and deliver the raw output data. 

'Solution.py' is written to solve the optimization problem for the full year with an optimization horizon of 1 day (24 hours) and deliver the results as requested in bullets 4-6 of the project specifications. 
	The results for bullet 4 are written out to a csv file named 'bullet4.csv'. 
	The results for bullet 5 are printed to the screen. 
	The results for bullet 6 are saved as 'Bullet_6a.png' and 'Bullet_6b.png'.

The remaining files and directories were used as learning materials and development tools and can be ignored. These include:
	'PyomoGettingStarted-master'
	'PyomoPractice'
	'DataParserPlayground.py'
	'playground.py'
	'SimplifiedModel_Solver.py'
	'testdata.csv'
	'*.lp'

There are many areas for improvement in this code and if given the opportunity to present I would be more than happy to discuss all the changes I would implement were this a true development project. 
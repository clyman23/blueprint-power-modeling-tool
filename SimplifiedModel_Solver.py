# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 10:17:01 2018

@author: clyma
"""

import pyomo.environ as pyo

model = pyo.ConcreteModel()

horizon = 24

model.T = pyo.RangeSet(1,horizon)

cost = {1:33.6, 2:32.05, 3:29.23, 4:25.02, 5:26.94, 6:25.62, 7:24.03, 8:27.36,\
        9:30.58, 10:35.4, 11:31.5, 12:30.45, 13:31.78, 14:29.96, 15:30.08, 16:30.09,\
        17:35.3, 18:45.54, 19:40.93, 20:39.6, 21:37.35, 22:35.54, 23:31.84, 24:29.81}

model.p = pyo.Param(model.T, initialize=cost, default = 0)

model.d = pyo.Var(model.T, domain=pyo.NonNegativeReals, initialize=0)

model.c = pyo.Var(model.T, domain=pyo.NonNegativeReals, initialize=0)

model.s = pyo.Var(model.T, domain=pyo.NonNegativeReals, initialize=0)

def d_constraint1(model, t):
    max_d = 200
    return model.d[t] <= max_d and model.d[t] >= 0

def d_constraint2(model, t):
    max_daily_d = 200
    return pyo.summation(model.d) <= max_daily_d 

model.d_con1 = pyo.Constraint(model.T, rule=d_constraint1)
model.d_con2 = pyo.Constraint(model.T, rule=d_constraint2)

def c_constraint1(model, t):
    max_c = 100
    return model.c[t] <= max_c and model.c[t]>=0

model.c_con1 = pyo.Constraint(model.T, rule=c_constraint1)

def s_constraint1(model, t):
    efficiency = 0.85
    if t == 1:
        return model.s[t] == efficiency*model.c[t]-model.d[t]
    elif t == 24:
        return model.s[t-1]+efficiency*model.c[t]-model.d[t] == model.s[1]
    return model.s[t] == model.s[t-1]+efficiency*model.c[t]-model.d[t]

model.s_con1 = pyo.Constraint(model.T, rule=s_constraint1)

#def s_constraint2(model, t):
    

model.OBJ = pyo.Objective(expr=sum(model.p[t]*(model.d[t]-model.c[t]) for t in model.T), \
                          sense=pyo.maximize)

#instance = model.create_instance() # DEPRECATED!!!
model.pprint()

opt = pyo.SolverFactory('glpk')
results = opt.solve(model)
results.write()

#pyo.SolverFactory('glpk').solve(model).write()

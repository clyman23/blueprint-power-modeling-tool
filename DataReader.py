# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 19:01:13 2018

@author: clyma
"""

import os
import csv

class ImportData:
    
    def __init__(self):
        self.file_location = os.getcwd()
        
    def read_data(self, filename):
        file = os.path.join(self.file_location, filename)
        contents = []
        with open(file, 'r') as fh:
            csv_file = csv.reader(fh)
            for r in csv_file:
                contents.extend(float(i) for i in r)
            #contents_dict = {index: float(x) for index, x in enumerate(contents, start=1)}
        return contents
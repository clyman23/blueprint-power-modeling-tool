# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 19:07:22 2018

@author: clyma
"""

from Model_Solver import Horizon_Solver
from Model_Solver import pyo
from DataReader import ImportData


class Full_Solver:
    
    def __init__(self, horizon):
        self.data = self.Cost_Data()
        self.horizon = horizon

    def Cost_Data(self):
        data_reader = ImportData()
        print("Importing data...")
        data = data_reader.read_data('All_Data.csv')
        print("Data imported!")
        return data
    
    def Solve_Day(self, multiplier, initial_values):
        #cost = self.data[24*(multiplier-1):24*multiplier]
        cost = {key:self.data[key-1] for key in range(1+24*(multiplier-1),1+24*multiplier)}
        #initial_values = {'s': 0, 'd': 0 , 'c': 0}
        #print("Solving...")
        solver = Horizon_Solver(self.horizon, cost, initial_values, multiplier)
        
        model = solver.create_model()
        
        solved = solver.solve_model(model)
        
        end_of_day = self.daily_results(solved, model)
        '''print()
        print()
        print('$'+str(end_of_day['r']))'''
        return end_of_day
        
    def daily_results(self, solved, model):
        power_output = [pyo.value(solved.d[i]) for i in solved.d]
        state_of_e = [pyo.value(solved.s[i]) for i in solved.s]
        c = [pyo.value(solved.c[i]) for i in solved.c]
        revenue = pyo.value(model.OBJ)
        revenue = round(revenue, 2)
        # Total annual charging cost needed
        return {'d':power_output, 's':state_of_e, \
                    'r':revenue, 'c': c}
    
    
    def Solve_Week(self):
        day = 1
        initial_values = {'s':0, 'd':0, 'c':0}
        revenue = 0
        week_data = {'s':[], 'c':[], 'd':[], 'r':[]}
        for i in range(1,8):
            #hour = 24*day
            daily_data = self.Solve_Day(day, initial_values)
            week_data['s'].extend(daily_data['s'])
            week_data['c'].extend(daily_data['c'])
            week_data['d'].extend(daily_data['d'])
            week_data['r'].append(daily_data['r'])
            initial_values = {'s':daily_data['s'][-1], 'd':daily_data['d'][-1],\
                              'c':daily_data['c'][-1]}
            revenue += daily_data['r']
            day += 1
        return week_data
    
    def Solve_Year(self):
        day = 1
        initial_values = {'s':0, 'd':0, 'c':0}
        revenue = 0
        year_data = {'s':[], 'c':[], 'd':[], 'r':[]}
        print("Solving...")
        for i in range(1,366):
            #hour = 24*day
            if day % 10 == 0:
                print("Solving day",str(day))
            daily_data = self.Solve_Day(day, initial_values)
            year_data['s'].extend(daily_data['s'])
            year_data['c'].extend(daily_data['c'])
            year_data['d'].extend(daily_data['d'])
            year_data['r'].append(daily_data['r'])
            initial_values = {'s':daily_data['s'][-1], 'd':daily_data['d'][-1],\
                              'c':daily_data['c'][-1]}
            revenue += daily_data['r']
            day += 1
        return year_data






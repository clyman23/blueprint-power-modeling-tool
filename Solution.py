# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 18:19:12 2018

@author: clyma
"""

import Full_Solver as fs
import matplotlib.pyplot as plt
import csv
import os

class Solution():
    
    def __init__(self):
        self.solution, self.full_solve = self.Solve()
        self.revenue = self.solution['r']
        self.state_of_energy = self.solution['s']
        self.power_output = self.solution['d']
        self.charging = self.solution['c']

    def Solve(self):
        full_solve = fs.Full_Solver(24)
        solution = full_solve.Solve_Year()
        return solution, full_solve
    
    def Display_Results(self):
        self.Bullet4()
        self.Bullet5()
        self.Bullet6a()
        self.Bullet6b()

    def Bullet4(self):
        bullet4 = {'power output (kW)': self.power_output, \
                   'state of energy (kWh)': self.state_of_energy}
        file_name = 'bullet4.csv'
        location = os.path.join(os.getcwd(),file_name)
        with open(location, 'w') as fh:
            writer = csv.writer(fh)
            writer.writerow(bullet4.keys())
            writer.writerows(zip(*bullet4.values()))

    def Bullet5(self):
        cost_data = self.full_solve.data
        # Total annual revenue generation
        total_rev = round(sum(self.revenue),2)
        print("Total revenue = $" + str(total_rev))
        # Total annual charging cost
        total_charging_cost = round(sum([a*(b/1000) for a,b in zip(cost_data, self.charging)]),2)
        print("Total charging cost = $" + str(total_charging_cost))
        # Total annual discharged throughput
        total_discharged = round(sum(self.power_output))
        print("Total annual discharged throughput = " + str(total_discharged) + " kWh")


    def Bullet6a(self):
        week_revs = []
        # 7 days a week with 365 days in a year means last day will be ignored
        for i in range(0,len(self.revenue),7):
            #print(i)
            week_rev = sum(self.revenue[i:i+7])
            week_revs.append(week_rev)
            
        max_weekly_revenue = max(week_revs)
        max_week = week_revs.index(max_weekly_revenue)
        # Data is 0 indexed, so will have to subtract by 1 later
        max_week_end = max_week*7*24 
        max_week_start = max_week_end - (7*24) + 1
        
        max_week_LBMP = self.full_solve.data[max_week_start-1:max_week_end]
        max_week_dispatch = self.power_output[max_week_start-1:max_week_end]
        
        day_array = ['Day 1', 'Day 2', 'Day 3', 'Day 4', 'Day 5', 'Day 6', 'Day 7']
        figa, ax1a = plt.subplots()
        x = [t for t in range(1,7*24+1)]
        ax1a.plot(x, max_week_LBMP, 'b')
        ax1a.set_xlabel('Time (hr)')
        plt.xticks([t for t in range(1,7*24+1,24)], day_array)
        ax1a.set_ylabel('LBMP ($/MWh)')
        ax2a = ax1a.twinx()
        ax2a.plot(x, max_week_dispatch, 'g')
        ax2a.set_ylabel('Hourly battery dispatch (kW)')
        plt.savefig('Bullet_6a.png')

    def Bullet6b(self):
        month_rev = self.Month_Rev()
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',\
                  'Oct', 'Nov', 'Dec']
        figb, axb = plt.subplots()
        num_months = range(12)
        bars = axb.bar(num_months, month_rev)
        axb.set_xlabel('Month')
        axb.set_ylabel('Monthly Revenue ($)')
        axb.set_title('Monthly Revenue')
        axb.set_xticks(num_months)
        axb.set_xticklabels(months)
        plt.savefig('Bullet6b.png')
        
    def Month_Rev(self):
        months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        month_days = [sum(months[:x+1]) for x in range(0,len(months))]
        month_rev = [sum(self.revenue[:month_days[0]+1])]
        for i in range(0,len(month_days)-1):
            print(month_days[i], end=' ')
            print(month_days[i+1])
            month_rev.append(sum(self.revenue[month_days[i]:month_days[i+1]]))
        month_rev = [round(i, 2) for i in month_rev]
        return month_rev

def main():
    print("Hello!")
    print("Solving energy arbitrage...")
    s = Solution()
    s.Display_Results()
    
if __name__ == '__main__':
    main()


{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#8. Data Input"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pyomo can initialize models in two general ways. When executing the <span style=\"color:darkblue; font-family:Courier\">pyomo</span> command, one or more data command files can be specified to declare data and load data from other data sources (e.g. spreadsheets and CSV files). When initializing a model within a Python script, a <span style=\"color:darkblue; font-family:Courier\">DataPortal</span> object can be used to load data from one or more data sources.\n",
    "\n",
    "###8.1. Data Command Files\n",
    "The following commands can be used in data command files:\n",
    "\n",
    "- <span style=\"color:darkblue; font-family:Courier\">set</span> declares set data,\n",
    "\n",
    "- <span style=\"color:darkblue; font-family:Courier\">param</span> declares a table of parameter data, which can also include the declaration of the set data used to index parameter data,\n",
    "\n",
    "- <span style=\"color:darkblue; font-family:Courier\">load</span> loads set and parameter data from an external data source such as ASCII table files, CSV files, ranges in spreadsheets, and database tables,\n",
    "\n",
    "- <span style=\"color:darkblue; font-family:Courier\">table</span> loads set and parameter data from a table,\n",
    "\n",
    "- <span style=\"color:darkblue; font-family:Courier\">include</span> specifies a data command file that is to be processed immediately,\n",
    "\n",
    "- the <span style=\"color:darkblue; font-family:Courier\">data</span> and <span style=\"color:darkblue; font-family:Courier\">end</span> commands do not perform any actions, but they provide compatibility with AMPL scripts that define data commands, and\n",
    "\n",
    "- <span style=\"color:darkblue; font-family:Courier\">namespace</span> defines groupings of data commands.\n",
    "\n",
    "The syntax of the <span style=\"color:darkblue; font-family:Courier\">set</span> and <span style=\"color:darkblue; font-family:Courier\">param</span> data commands are adapted from AMPL’s data commands. However, other Pyomo data commands do not directly correspond to AMPL data commands. In particular, Pyomo’s <span style=\"color:darkblue; font-family:Courier\">table</span> command was introduced to work around semantic ambiguities in the <span style=\"color:darkblue; font-family:Courier\">param</span> command. Pyomo’s <span style=\"color:darkblue; font-family:Courier\">table</span> command does not correspond to AMPL’s <span style=\"color:darkblue; font-family:Courier\">table</span> command. Instead, the <span style=\"color:darkblue; font-family:Courier\">load</span> command mimics AMPL’s <span style=\"color:darkblue; font-family:Courier\">table</span> command with a simplified syntax.\n",
    "\n",
    ">####Warning:\n",
    ">The data command file was initially developed to provide compatability in data formats between Pyomo and AMPL. However, these data formats continue to diverge in their syntax and semantics. Simple examples using <span style=\"color:darkblue; font-family:Courier\">set</span> and <span style=\"color:darkblue; font-family:Courier\">param</span> data commands are likely to work for both AMPL and Pyomo, particularly with abstract Pyomo models. But in general a user should expect to need to adapt their AMPL data command files for use with Pyomo.\n",
    "\n",
    "See the [PyomoBook](https://software.sandia.gov/downloads/pub/pyomo/PyomoOnlineDocs.html#PyomoBook) book for detailed descriptions of these commands. The following sections provide additional details, particularly for new data commands that are not described in the [PyomoBook](https://software.sandia.gov/downloads/pub/pyomo/PyomoOnlineDocs.html#PyomoBook) book: <span style=\"color:darkblue; font-family:Courier\">table</span>.\n",
    "\n",
    "####8.1.1. table\n",
    "\n",
    "The <span style=\"color:darkblue; font-family:Courier\">table</span> data command was developed to provide a more flexible and complete data declaration than is possible with the <span style=\"color:darkblue; font-family:Courier\">param</span> declaration. This command has a similar syntax to the <span style=\"color:darkblue; font-family:Courier\">load</span> command, but it includes a complete specification of the table data.\n",
    "\n",
    "The following example illustrates a simple <span style=\"color:darkblue; font-family:Courier\">table</span> command that declares data for a single parameter:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table M(A) :\n",
    "A  B  M   N :=\n",
    "A1 B1 4.3 5.3\n",
    "A2 B2 4.4 5.4\n",
    "A3 B3 4.5 5.5\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameter <span style=\"color:darkblue; font-family:Courier\">M</span> is indexed by column <span style=\"color:darkblue; font-family:Courier\">A</span>. The column labels are provided after the colon and before the <span style=\"color:darkblue; font-family:Courier\">:=</span>. Subsequently, the table data is provided. Note that the syntax is not sensitive to whitespace. Thus, the following is an equivalent <span style=\"color:darkblue; font-family:Courier\">table</span> command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table M(A) :\n",
    "A  B  M   N :=\n",
    "A1 B1 4.3 5.3 A2 B2 4.4 5.4 A3 B3 4.5 5.5 ;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple parameters can be declared by simply including additional parameter names. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table M(A) N(A,B) :\n",
    "A  B  M   N :=\n",
    "A1 B1 4.3 5.3\n",
    "A2 B2 4.4 5.4\n",
    "A3 B3 4.5 5.5\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example declares data for the <span style=\"color:darkblue; font-family:Courier\">M</span> and <span style=\"color:darkblue; font-family:Courier\">N</span> parameters. As this example illustrates, these parameters may have different indexing columns.\n",
    "\n",
    "The indexing columns represent set data, which is specified separately. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table A={A} Z={A,B} M(A) N(A,B) :\n",
    "A  B  M   N :=\n",
    "A1 B1 4.3 5.3\n",
    "A2 B2 4.4 5.4\n",
    "A3 B3 4.5 5.5\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This examples declares data for the <span style=\"color:darkblue; font-family:Courier\">M</span> and <span style=\"color:darkblue; font-family:Courier\">N</span> parameters, along with the <span style=\"color:darkblue; font-family:Courier\">A</span> and <span style=\"color:darkblue; font-family:Courier\">Z</span> indexing sets. The correspondence between the index set <span style=\"color:darkblue; font-family:Courier\">Z</span> and the indices of parameter <span style=\"color:darkblue; font-family:Courier\">N</span>can be made more explicit by indexing <span style=\"color:darkblue; font-family:Courier\">N</span> by <span style=\"color:darkblue; font-family:Courier\">Z</span>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table A={A} Z={A,B} M(A) N(Z) :\n",
    "A  B  M   N :=\n",
    "A1 B1 4.3 5.3\n",
    "A2 B2 4.4 5.4\n",
    "A3 B3 4.5 5.5\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set data can also be specified independent of parameter data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table Z={A,B} Y={M,N} :\n",
    "A  B  M   N :=\n",
    "A1 B1 4.3 5.3\n",
    "A2 B2 4.4 5.4\n",
    "A3 B3 4.5 5.5\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, singleton parameter values can be specified with a simple <span style=\"color:darkblue; font-family:Courier\">table</span> command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table pi := 3.1416 ;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The previous examples considered examples of the <span style=\"color:darkblue; font-family:Courier\">table</span> command where column labels are provided. The <span style=\"color:darkblue; font-family:Courier\">table</span> command can also be used without column labels. For example, the file [table0.dat](https://software.sandia.gov/downloads/pub/pyomo/PyomoOnlineDocs.html#table0.dat) can be revised to omit column labels as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table columns=4 M(1)={3} :=\n",
    "A1 B1 4.3 5.3\n",
    "A2 B2 4.4 5.4\n",
    "A3 B3 4.5 5.5\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <span style=\"color:darkblue; font-family:Courier\">columns=4</span> is a keyword-value pair that defines the number of columns in this table; this must be explicitly specified in unlabeled tables. The default column labels are integers starting from <span style=\"color:darkblue; font-family:Courier\">1;</span> the labels are columns <span style=\"color:darkblue; font-family:Courier\">1<span>, <span style=\"color:darkblue; font-family:Courier\">2</span>, <span style=\"color:darkblue; font-family:Courier\">3</span>, and <span style=\"color:darkblue; font-family:Courier\">4</span> in this example. The M parameter is indexed by column <span style=\"color:darkblue; font-family:Courier\">1</span>. The braces syntax declares the column where the <span style=\"color:darkblue; font-family:Courier\">M</span> data is provided.\n",
    "\n",
    "Similarly, set data can be declared referencing the integer column labels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table A={1} Z={1,2} M(1) N(1,2) :=\n",
    "A1 B1 4.3 5.3\n",
    "A2 B2 4.4 5.4\n",
    "A3 B3 4.5 5.5\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Declared set names can also be used to index parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table A={1} Z={1,2} M(A) N(Z) :=\n",
    "A1 B1 4.3 5.3\n",
    "A2 B2 4.4 5.4\n",
    "A3 B3 4.5 5.5\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we compare and contrast the <span style=\"color:darkblue; font-family:Courier\">table</span> and <span style=\"color:darkblue; font-family:Courier\">param</span> commands:\n",
    "\n",
    "- Both commands can be used to declare parameter and set data.\n",
    "\n",
    "- The <span style=\"color:darkblue; font-family:Courier\">param</span> command can declare a single set that is used to index one or more parameters. The <span style=\"color:darkblue; font-family:Courier\">table</span> command can declare data for any number of sets, independent of whether they are used to index parameter data.\n",
    "\n",
    "- The <span style=\"color:darkblue; font-family:Courier\">param</span> command can declare data for multiple parameters only if they share the same index set. The <span style=\"color:darkblue; font-family:Courier\">table</span> command can declare data for any number of parameters that are may be indexed separately.\n",
    "\n",
    "- Both commands can be used to declare a singleton parameter.\n",
    "\n",
    "- The <span style=\"color:darkblue; font-family:Courier\">table</span> syntax unambiguously describes the dimensionality of indexing sets. The <span style=\"color:darkblue; font-family:Courier\">param</span> command must be interpreted with a model that provides the dimension of the indexing set.\n",
    "\n",
    "This last point provides a key motivation for the <span style=\"color:darkblue; font-family:Courier\">table</span> command. Specifically, the <span style=\"color:darkblue; font-family:Courier\">table</span> command can be used to reliably initialize concrete models using a <span style=\"color:darkblue; font-family:Courier\">DataPortal</span> object. By contrast, the <span style=\"color:darkblue; font-family:Courier\">param</span> command can only be used to initialize concrete models with parameters that are indexed by a single column (i.e. a simple set). See the discussion of <span style=\"color:darkblue; font-family:Courier\">DataPortal</span> objects below for an example.\n",
    "\n",
    "#####8.1.2. namespace\n",
    "\n",
    "The <span style=\"color:darkblue; font-family:Courier\">namespace</span> command allows data commands to be organized into named groups that can be enabled from the pyomo command line. For example, consider again the [abstract2.py](https://software.sandia.gov/downloads/pub/pyomo/PyomoOnlineDocs.html#abstract2.py) example. Suppose that the cost data shown in [abstract2.dat](https://software.sandia.gov/downloads/pub/pyomo/PyomoOnlineDocs.html#abstract2.dat) were valid only under certain circumstances that we will label as \"TerryG\" and that there would be different cost data under circumstances that we will label \"JohnD.\" This could be represented using the following data file:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# abs2nspace.dat AMPL format with namespaces\n",
    "\n",
    "set I := TV Film ;\n",
    "set J := Graham John Carol ;\n",
    "\n",
    "param a :=\n",
    "TV  Graham 3\n",
    "TV John 4.4\n",
    "TV Carol 4.9\n",
    "Film Graham 1\n",
    "Film John 2.4\n",
    "Film Carol 1.1\n",
    ";\n",
    "\n",
    "namespace TerryG {\n",
    "   param c := [*]\n",
    "     Graham 2.2\n",
    "     John 3.1416\n",
    "     Carol 3\n",
    "   ;\n",
    "}\n",
    "\n",
    "namespace JohnD {\n",
    "   param c := [*]\n",
    "     Graham 2.7\n",
    "     John 3\n",
    "     Carol 2.1\n",
    "   ;\n",
    "}\n",
    "\n",
    "param b := TV 1 Film 1 ;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To use this data file with [abstract2.py](https://software.sandia.gov/downloads/pub/pyomo/PyomoOnlineDocs.html#abstract2.py), a namespace must be indicated on the command line. To select the \"TerryG\" data specification, <span style=\"color:darkblue; font-family:Courier\">--namespace TerryG</span> would be added to the command line. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyomo solve abstract2.py abs2nspace.dat --namespace TerryG --solver=cplex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the <span style=\"color:darkblue; font-family:Courier\">--namespace</span> option is omitted, then no data will be given for <span style=\"color:darkblue; font-family:Courier\">model.c</span> (and no default was given for <span style=\"color:darkblue; font-family:Courier\">model.c</span>). In other words, there is no default namespace selection.\n",
    "\n",
    "The option <span style=\"color:darkblue; font-family:Courier\">-ns</span> (with one dash) is an alias for <span style=\"color:darkblue; font-family:Courier\">--namespace</span> (which needs two dashes) Multiple namespaces can be selected by giving multiple <span style=\"color:darkblue; font-family:Courier\">--namespace</span> or <span style=\"color:darkblue; font-family:Courier\">-ns</span> arguments on the Pyomo command line.\n",
    "\n",
    "##8.2. DataPortal Objects\n",
    "The <span style=\"color:darkblue; font-family:Courier\">load</span> and <span style=\"color:darkblue; font-family:Courier\">store</span> Pyomo data commands can be used to load set and table data from a variety of data sources. Pyomo’s <span style=\"color:darkblue; font-family:Courier\">DataPortal</span> object provides this same functionality for users who work with Python scripts. A <span style=\"color:darkblue; font-family:Courier\">DataPortal</span> object manages the process of loading data from different data sources, and it is used to construct model instances in a standard manner. Similarly, a <span style=\"color:darkblue; font-family:Courier\">DataPortal</span> object can be used to store model data externally in a standard manner.\n",
    "\n",
    "####8.2.1. Loading Data\n",
    "\n",
    "The <span style=\"color:darkblue; font-family:Courier\">load</span> method can be used to load data into Pyomo models from a variety of sources and formats. The most common format is a table representation of set and parameter data. For example, consider the file <span style=\"color:darkblue; font-family:Courier\">A.tab</span>, which defines a simple set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A\n",
    "A1\n",
    "A2\n",
    "A3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following example illustrates how a <span style=\"color:darkblue; font-family:Courier\">DataPortal</span> object can be used to load this data into a model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = AbstractModel()\n",
    "model.A = Set()\n",
    "\n",
    "data = DataPortal()\n",
    "data.load(filename='tab/A.tab', set=model.A)\n",
    "instance = model.create(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <span style=\"color:darkblue; font-family:Courier\">load</span> method opens the data file, processes it, and loads the data in a format that is then used to construct a model instance. The <span style=\"color:darkblue; font-family:Courier\">load</span> method can be called multiple times to load data for different sets or parameters, or to override data processed earlier.\n",
    "\n",
    ">#####Note\n",
    ">Subsequent examples omit the model declaration and instance creation.\n",
    "\n",
    "In the previous example, the <span style=\"color:darkblue; font-family:Courier\">set</span> option is used to define the model component that is loaded with the set data. If the data source defines a table of data, then this option is used to specify data for a multi-dimensional set. For example, consider the file <span style=\"color:darkblue; font-family:Courier\">D.tab</span>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A  B\n",
    "A1 1\n",
    "A1 2\n",
    "A1 3\n",
    "A2 1\n",
    "A2 2\n",
    "A2 3\n",
    "A3 1\n",
    "A3 2\n",
    "A3 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If a two-dimensional set is declared, then it can be loaded with the same syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.A = Set(dimen=2)\n",
    "\n",
    "data.load(filename='tab/C.tab', set=model.A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example also illustrates that the column titles do not directly impact the process of loading data. Column titles are only used to select columns that are included in the table that is loaded (see below).\n",
    "\n",
    "The <span style=\"color:darkblue; font-family:Courier\">param</span> option is used to define the a parameter component that is loaded with data. The simplest parameter is a singleton. For example, consider the file <span style=\"color:darkblue; font-family:Courier\">Z.tab</span>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "1.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This data is loaded with the following syntax:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.z = Param()\n",
    "\n",
    "data.load(filename='tab/Z.tab', param=model.z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Indexed parameters can be defined from table data. For example, consider the file <span style=\"color:darkblue; font-family:Courier\">Y.tab</span>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A  Y\n",
    "A1 3.3\n",
    "A2 3.4\n",
    "A3 3.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameter <span style=\"color:darkblue; font-family:Courier\">y</span> is loaded with the following syntax:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.A = Set(initialize=['A1','A2','A3','A4'])\n",
    "model.y = Param(model.A)\n",
    "\n",
    "data.load(filename='tab/Y.tab', param=model.y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pyomo assumes that the parameter values are defined on the rightmost column; the column names are not used to specify the index and parameter data (see below). In this file, the <span style=\"color:darkblue; font-family:Courier\">A</span> column contains the index values, and the <span style=\"color:darkblue; font-family:Courier\">Y</span> column contains the parameter values.\n",
    "\n",
    "Similarly, multiple parameters can be initialized at once by specifying a list (or tuple) of component parameters. For example, consider the file <span style=\"color:darkblue; font-family:Courier\">XW.tab</span>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A  X   W\n",
    "A1 3.3 4.3\n",
    "A2 3.4 4.4\n",
    "A3 3.5 4.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameters <span style=\"color:darkblue; font-family:Courier\">x</span> and <span style=\"color:darkblue; font-family:Courier\">w</span> are loaded with the following syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.A = Set(initialize=['A1','A2','A3','A4'])\n",
    "model.x = Param(model.A)\n",
    "model.w = Param(model.A)\n",
    "\n",
    "data.load(filename='tab/XW.tab', param=(model.x,model.w))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the data for set <span style=\"color:darkblue; font-family:Courier\">A</span> is predefined in this example. The index set can be loaded with the parameter data using the <span style=\"color:darkblue; font-family:Courier\">index</span> option:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.A = Set()\n",
    "model.x = Param(model.A)\n",
    "model.w = Param(model.A)\n",
    "\n",
    "data.load(filename='tab/XW.tab', param=(model.x,model.w), index=model.A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have previously noted that the column names are not used to define the set and parameter data. The <span style=\"color:darkblue; font-family:Courier\">select</span> option is used to define the columns in the table that are used to load data. This option specifies a list (or tuple) of column names that are used, in that order, to form the table that defines the component data.\n",
    "\n",
    "For example, consider the following load declaration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.A = Set()\n",
    "model.w = Param(model.A)\n",
    "\n",
    "data.load(filename='tab/XW.tab', select=('A','W'), param=model.w, index=model.A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The columns <span style=\"color:darkblue; font-family:Courier\">A</span> and <span style=\"color:darkblue; font-family:Courier\">W</span> are selected from the file XW.tab, and a single parameter is defined.\n",
    "\n",
    ">#####Note\n",
    ">The <span style=\"color:darkblue; font-family:Courier\">load</span> method allows for a variety of other options that are supported by the add method for <span style=\"color:darkblue; font-family:Courier\">ModelData</span> objects. See the [PyomoBook](https://software.sandia.gov/downloads/pub/pyomo/PyomoOnlineDocs.html#PyomoBook) book for a detailed description of these options."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}

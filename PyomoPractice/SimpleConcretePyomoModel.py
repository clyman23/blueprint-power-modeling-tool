# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 21:45:13 2018

@author: clyma
"""

from __future__ import division
import pyomo.environ as pyo

model = pyo.ConcreteModel()

model.y = pyo.Var(domain=pyo.NonNegativeReals)

model.profit = pyo.Objective(expr=90*model.y, sense=pyo.maximize)

model.laborA = pyo.Constraint(expr=model.y <= 80)
model.laborB = pyo.Constraint(expr=model.y <= 100)

pyo.SolverFactory('glpk').solve(model).write()
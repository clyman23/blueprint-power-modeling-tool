# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 21:24:15 2018

Pyomo practice from 
https://pyomo.readthedocs.io/en/latest/pyomo_overview/simple_examples.html

@author: clyma
"""

#Ensure int or long division argurments are converted to floats before division performed
from __future__ import division
#Make symbols used by Pyomo known to Python
from pyomo.environ import *

#declare model
model = AbstractModel()

#Declare m and n using Param
#Within used to validate the data value assigned to the param
model.m = Param(within=NonNegativeIntegers)
model.n = Param(within=NonNegativeIntegers)
#Assignment value that is not non-negative integer will give error

#Convenient to define index sets
#RangeSet used to declare sets will be sequence of ints from 1 to ending value
model.I = RangeSet(1, model.m)
model.J = RangeSet(1, model.n)

#When sets given to Param, they indicate that the set will index the param
model.a = Param(model.I, model.J)
model.b = Param(model.I)
model.c = Param(model.J)

#Declare variable X
#First argument to Var is a set, so it is defined as an index set for the variable
#Multiple index sets can be used
#Second argument specifies domain for variable
model.x = Var(model.J, domain=NonNegativeReals)
#We have delcared a variable x indexed by the set J
#NonNegativeReals means variable be greater than or equal to 0

#Pyomo summation function
#Returns expression for sum of product of two arguments over their indices
#The two arguments must have the same indicies
def obj_expression(model):
    return summation(model.c, model.x)

#Declare objective function
#rule argument gives the name of the function that returns the expression to be used
#Default "sense" is minimization
#To maximize, use sense=maximize
    
model.OBJ = Objective(rule=obj_expression)

def ax_constraint_rule(model, i):
    return sum(model.a[i, j] * model.x[j] for j in model.J) >= model.b[i]

model.AxbConstraint = Constraint(model.I, rule=ax_constraint_rule)










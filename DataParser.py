# -*- coding: utf-8 -*-
"""
Created on Sat Nov  3 12:29:18 2018

Parse out energy cost data for NYC

@author: clyma
"""
import os
import csv
import matplotlib.pyplot as plt

def read_csv(file):
    data = []
    with open(file, 'r') as csvfile:
        contents = csv.DictReader(csvfile)
        for row in contents:
            if row['Name'] == 'N.Y.C.':
                data.append(row)
    return data

def get_files(location):
    files = os.listdir(location)
    try: 
        files.remove('.DS_Store')
    except:
        pass
    for i in range(0,len(files)):
        files[i] = os.path.join(location, files[i])
    return files

def get_file_dirs(location):
    dirs = os.listdir(location)
    try:
        dirs.remove('.DS_Store')
    except:
        pass
    for i in range(0,len(dirs)):
        dirs[i] = os.path.join(location, dirs[i])
    return dirs

def read_csvs(files):
    all_data = []
    for i in files:
        all_data.append(read_csv(i))
    return all_data

def extract_NYC_data(file_location):
    dirs = get_file_dirs(file_location)
    all_data = []
    all_files = []
    for i in dirs:
        all_files.extend(get_files(i))
    for i in all_files:
        all_data.extend(read_csv(i))
    return all_data

def plot_data(all_data):
    time = []
    cost = []
    for i in all_data:
        time.append(i.get('Time Stamp'))
        cost.append(float(i.get('LBMP ($/MWHr)')))
    print("Data sorted...")
    plt.plot(cost)
    plt.show()


file_location = r'C:\Users\clyma\Documents\BlueprintPower_Challenge\2017_NYISO_LBMPs'
        
#dirs = get_file_dirs(file_location)
#files = get_files(dirs[0])
#data = read_csvs(files)
all_data = extract_NYC_data(file_location)
plot_data(all_data)
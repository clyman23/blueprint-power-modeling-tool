import Full_Solver as fs
import matplotlib.pyplot as plt
import csv
import os

full_solve = fs.Full_Solver(24)
data = full_solve.Solve_Year()

cost_data = full_solve.data 
charging = data['s']
total_charging_cost = round(sum([a*(b/1000) for a,b in zip(cost_data, charging)]),2)
print("Total charging cost = $" + str(total_charging_cost))
        

max_rev = max(data['r'])

print(max_rev)

max_rev_week = data['r'].index(max_rev) + 1
max_day_start = (max_rev_week - 1) * 7 + 1
max_day_end = max_rev_week * 7

power_output = data['d']
state_of_energy = data['s']
total_rev = round(sum(data['r']),2)
print("Total revenue = $" + str(total_rev))

total_discharged = round(sum(power_output))
print("Total annual discharged throughput = " + str(total_discharged) + " kWh")

#s_max_week = max_rev['s']

def Bullet4(power_output, state_of_energy):
    bullet4 = {'power output (kW)': power_output, 'state of energy (kWh)': state_of_energy}
    file_name = 'bullet4.csv'
    location = os.path.join(os.getcwd(),file_name)
    with open(location, 'w') as fh:
        writer = csv.writer(fh)
        writer.writerow(bullet4.keys())
        writer.writerows(zip(*bullet4.values()))